/* eslint-disable */

import React from 'react'
import { hot } from 'react-hot-loader'
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom'
import Header from './Components/Header'
import Footer from './Components/Footer'
import ProductList from './Components/ProductList'
import ProductDetails from './Components/ProductDetails'
import Cart from './Components/Cart'

class App extends React.Component {
  render() {
    return (
      <div className="page-container">
        <Router>
          <Header />
          {/* <Sidebar /> */}

          <Switch>
            <Route path="/" exact component={ProductList} />
            <Route path="/details" component={ProductDetails} />
            <Route path="/cart" component={Cart} />
          </Switch>
        </Router>

        <Footer />
      </div>
    )
  }
}

export default hot(module)(App)
