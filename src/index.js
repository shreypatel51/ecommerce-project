import React from 'react'
import ReactDOM from 'react-dom'
import 'core-js'
import App from './App'
import './styles.css'
import { ItemProvider } from './Components/Context'

if (process.env.NODE_ENV === 'development') {
  const axe = require('react-axe')
  axe(React, ReactDOM, 1000)
}

ReactDOM.render(
  <ItemProvider>
    <App />
  </ItemProvider>,
  document.getElementById('app')
)
