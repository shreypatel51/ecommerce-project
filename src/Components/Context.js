/* eslint-disable react/prop-types */
/* eslint-disable no-console */
import React, { Component } from 'react'
import axios from 'axios'

const ItemContext = React.createContext()
//Provider
//Consumer

class ItemProvider extends Component {
  constructor(props) {
    super(props)

    this.state = {
      items: [],
      itemsDetail: [],
      cart: [],
      handleDetails: this.handleDetails,
      addToCart: this.addToCart,
      handleSize: this.handleSize,
      handleColor: this.handleColor
    }
  }

  componentDidMount() {
    axios.get('http://localhost:4444/items').then(res => {
      this.setState({ items: res.data })
    })
  }

  getItem = id => {
    const product = this.state.items.find(item => item.tcin === id)
    return product
  }

  handleDetails = id => {
    const product = this.getItem(id)
    this.setState(() => {
      return { itemsDetail: product }
    })
  }

  addToCart = (id, selectedColor, selectedSize) => {
    let tempProducts = [...this.state.items]
    const index = tempProducts.indexOf(this.getItem(id))
    const product = tempProducts[index]

    var userObj = {
      selectedColor: selectedColor,
      selectedSize: selectedSize
    }
    let finishedProduct = { ...product, ...userObj }
    this.state.cart.push(finishedProduct)
    console.log(this.state.cart)
  }

  render() {
    return (
      <ItemContext.Provider value={this.state}>
        {this.props.children}
      </ItemContext.Provider>
    )
  }
}

const ItemConsumer = ItemContext.Consumer

export { ItemProvider, ItemConsumer, ItemContext }
