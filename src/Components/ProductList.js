/* eslint-disable no-console */
import React, { Component } from 'react'
import Products from './Products'
import { ItemConsumer } from './Context'
import '../CSS/Product-list.css'
import Sidebar from './Sidebar'

export default class ProductList extends Component {
  render() {
    return (
      <div className="product-container">
        <div className="sidebar">
          <Sidebar />
        </div>

        <div className="product-list">
          <ItemConsumer>
            {value => {
              return value.items.map(products => {
                return <Products key={products.tcin} products={products} />
              })
            }}
          </ItemConsumer>
        </div>
      </div>
    )
  }
}
