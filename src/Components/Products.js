/* eslint-disable react/prop-types */
/* eslint-disable no-console */
import React, { Component } from 'react'
import '../CSS/Products.css'
import { FaRegHeart } from 'react-icons/fa'
import { AiOutlineShoppingCart } from 'react-icons/ai'
import { Link } from 'react-router-dom'
import { ItemContext } from './Context'

export default class Products extends Component {
  render() {
    const { title, image, price, subscribable, tcin } = this.props.products
    // const productsContext = useContext(ItemContext)

    return (
      <ItemContext.Consumer>
        {value => (
          <div className="card">
            <div
              className="product-img"
              role="button"
              tabIndex={0}
              onClick={() => value.handleDetails(tcin)}
              onKeyDown={() => console.log('you pressed a key')}
            >
              <Link to={`/details?tcin=${tcin}`}>
                <img
                  className="img"
                  src={image}
                  alt="card pic"
                  loading="lazy"
                />
              </Link>
              <Link to={'/cart'}>
                <button
                  className="cart-btn"
                  disabled={subscribable ? true : false}
                  onClick={() => value.addToCart(tcin)}
                >
                  {subscribable ? (
                    <p disabled>In Cart</p>
                  ) : (
                    <AiOutlineShoppingCart />
                  )}
                </button>
              </Link>

              <button
                className="fav-btn"
                onClick={() => console.log('favorite button')}
              >
                <FaRegHeart />
              </button>
            </div>

            <div className="product-name">{title}</div>
            <div className="product-price">{price.formatted_current_price}</div>
          </div>
        )}
      </ItemContext.Consumer>
    )
  }
}
