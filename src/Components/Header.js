/* eslint-disable */

import React from 'react'
import '../header.css'
import { Link } from 'react-router-dom'
import { FaSearch } from 'react-icons/fa'
import { FaShoppingCart } from 'react-icons/fa'
import { IconContext } from 'react-icons'

export default class Header extends React.Component {
  render() {
    return (
      <div className="header">
        <header>
          <Link to="/">
            <img
              className="logo"
              src={
                "data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='42.996' height='32.879' viewBox='0 0 42.996 32.879'%3E%3Cpath d='M436.271,456.879a1.263,1.263,0,0,1-1.22-.814l-3.062-8.038H418.527A2.527,2.527,0,0,1,416,445.5V426.529A2.528,2.528,0,0,1,418.532,424h17.7a1.264,1.264,0,0,1,1.144.726l3.823,8.126h15.267A2.527,2.527,0,0,1,459,435.381V454.35a2.528,2.528,0,0,1-2.532,2.529Zm-.84-30.35h-16.9s0,6.323,0,18.969h25.828Zm6.961,8.852,5.1,10.842a1.265,1.265,0,0,1-.312,1.49L439.6,454.35h16.864s0-6.323,0-18.969Zm-5.624,18.085,6.216-5.439H434.7Z' transform='translate(-416 -424)'/%3E%3C/svg%3E"
              }
              alt="logo"
            />
          </Link>

          <h2>E-Shop</h2>
          <IconContext.Provider
            value={{ color: 'black', className: 'global-class-name' }}
          >
            <nav>
              <ul className="nav-links">
                <li>
                  <a href="#">
                    <FaSearch />
                  </a>
                </li>
                <li>
                  <Link to={'/cart'}>
                    <button className="cart-button">
                      <FaShoppingCart />
                    </button>
                  </Link>
                </li>
              </ul>
            </nav>
          </IconContext.Provider>
        </header>
      </div>
    )
  }
}
