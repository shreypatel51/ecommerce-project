import React, { Component } from 'react'
import '../CSS/Sidebar.css'

export default class Sidebar extends Component {
  render() {
    return (
      <div className="card-container">
        <div className="product-card">
          <span>Product Type</span>
          <ul className="product-type">
            <li>
              <input type="checkbox" name="sweatshirts" />
              <label htmlFor="sweatshirts"> Sweatshirts</label>
            </li>
            <li>
              <input type="checkbox" name="t-shirts" />
              <label htmlFor="t-shirts"> T-shirts</label>
            </li>
            <li>
              <input type="checkbox" name="tank-tops" />
              <label htmlFor="tank-tops"> Sweatshirts</label>
            </li>
            <li>
              <input type="checkbox" name="dress-shirts" />
              <label htmlFor="dress-shirts"> Dress shirts</label>
            </li>
          </ul>
        </div>

        <div className="slider-container">
          <span>Price</span>
          <div className="track">
            <div className="progress"></div>
          </div>

          <div className="thumb"></div>
        </div>
      </div>
    )
  }
}
