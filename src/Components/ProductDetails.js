/* eslint-disable jsx-a11y/no-onchange */
/* eslint-disable react/prop-types */
/* eslint-disable no-console */
import React, { Component } from 'react'
import '../CSS/Product-Details.css'
// import { Link } from 'react-router-dom'
import { ItemConsumer } from './Context'
import { Link } from 'react-router-dom'

export default class ProductDetails extends Component {
  constructor(props) {
    super(props)

    this.state = {
      selectedSize: 'default',
      selectedColor: 'default'
    }
  }

  handleSize = ({ selectedSize }) => {
    this.setState({ selectedSize })
  }

  handleColor = ({ selectedColor }) => {
    this.setState({ selectedColor })
  }

  render() {
    return (
      <div className="product-details-container">
        <ItemConsumer>
          {value => {
            const { title, image, price, description, tcin } = value.itemsDetail

            return (
              <div className="info-container">
                <div className="product-image">
                  <img
                    className="prod-image"
                    alt="details-img"
                    src={image}
                  ></img>
                </div>
                <div className="product-info">
                  <h2 className="title">{title}</h2>
                  <div className="details-price">
                    {price.formatted_current_price}
                  </div>

                  {/* SIZES */}
                  <div className="size">
                    <label htmlFor="sizes">Select a Size: </label>
                    <select
                      id="sizes"
                      onChange={e =>
                        this.handleSize({ selectedSize: e.target.value })
                      }
                      value={this.state.selectedSize}
                    >
                      <option value="select">Select</option>
                      <option value="extra-small">Extra Small</option>
                      <option value="small"> Small</option>
                      <option value="medium">Medium</option>
                      <option value="large">Large</option>
                      <option value="extra-large">Extra Large</option>
                    </select>
                  </div>

                  {/* COLORS */}

                  <div className="swatch">
                    <div>
                      {' '}
                      <span>Colors:</span>
                    </div>
                    {value.itemsDetail.swatches.color.map(color => (
                      <div className="colors" key={color.partNumber}>
                        <button
                          className="color-button"
                          onClick={() =>
                            this.handleColor({
                              selectedColor: color.color
                            })
                          }
                        >
                          <img
                            className="swatch-image"
                            alt={color.color}
                            src={color.swatch_url}
                          />
                        </button>
                      </div>
                    ))}
                  </div>

                  {/* ADD TO CART BUTTON */}

                  <div className="buttons">
                    <Link to={'/cart'}>
                      <button
                        className="crt-button"
                        onClick={() =>
                          value.addToCart(
                            tcin,
                            this.state.selectedColor,
                            this.state.selectedSize
                          )
                        }
                      >
                        Add to Cart
                      </button>
                    </Link>
                  </div>
                </div>

                <div className="break"></div>

                {/* DESCRIPTION */}
                <div className="description">
                  <button className="des-button">DESCRIPTION</button>
                  <div className="description-card">
                    <p>{description}</p>
                  </div>
                </div>
              </div>
            )
          }}
        </ItemConsumer>
      </div>
    )
  }
}
