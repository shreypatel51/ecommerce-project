import React, { Component } from 'react'
import '../CSS/Cart.css'
import { ItemConsumer } from './Context'

export default class Cart extends Component {
  render() {
    return (
      <ItemConsumer>
        {value => {
          // const { title, image, price } = value
          return (
            <div>
              {' '}
              <span className="shopping-cart">Shopping Cart</span>
              <div className="cart-container">
                {value.cart.map(item => (
                  <div className="cart-card" key={item.tcin}>
                    <ul id="cart-list">
                      <li className="cartImg">
                        <img
                          className="cart-image"
                          src={item.image}
                          alt={item.title}
                        />
                      </li>
                      <li className="info">
                        {item.title} #{item.tcin}
                        Size:{' '}
                        {item.selectedSize
                          ? item.selectedSize
                          : 'Size not Picked'}
                      </li>

                      <li>
                        Color:{' '}
                        {item.selectedColor
                          ? item.selectedColor
                          : 'Color not picked'}
                      </li>

                      <li>
                        <button className="remove-button">X</button>
                      </li>
                    </ul>
                  </div>
                ))}
              </div>
            </div>
          )
        }}
      </ItemConsumer>
    )
  }
}
