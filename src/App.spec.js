import React from 'react'
import { render } from '@testing-library/react'
import { ItemProvider } from './Components/Context'
import App from './App'

describe('App', () => {
  it('Renders without error', () => {
    render(
      <ItemProvider
        value={{
          items: []
        }}
      >
        <App />
      </ItemProvider>
    )
  })
})
