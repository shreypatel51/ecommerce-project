/* eslint-disable */

module.exports = {
  setupTestFrameworkScriptFile: '<rootDir>/testSetup.js',
  moduleNameMapper: {
    '\\.(css|sass)$': 'identity-obj-proxy'
  }
}
